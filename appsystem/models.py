from django.db import models


# Cette table permet de faire référence au système auquel appartient les corps ainsi que les différents systèmes
class System(models.Model):
    # Ce champ est la clé étrangère qui pointe vers l'id du système parent de la table système
    name = models.TextField(null=True)
    image = models.ImageField(null=True)
    allegiance = models.TextField(null=True)
    coordinates = models.TextField(null=True)
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, blank=True, null=True)
    population = models.IntegerField(null=True)
    ground_distance = models.IntegerField(null=True)
    state = models.TextField(null=True)
    economy = models.TextField(null=True)
    faction = models.TextField(null=True)
    security = models.TextField(null=True)
    government = models.TextField(null=True)

    def astronomical_bodies(self):
        return Celestial.objects.filter(fk_system_origine=self.id)

    def astronomical_bodies_count(self):
        return Celestial.objects.filter(fk_system_origine=self.id).count()

    def sub_systems(self):
        return System.objects.filter(parent_id=self.id)

    def sub_systems_count(self):
        return System.objects.filter(parent_id=self.id).count()

    def __str__(self):
        return f"{self.name}"

    # définition des variable à choix multiple

    # ALLEGEANCE_CHOICES = (
    # (1, ('Fédération')),
    # (2 ('Alliance')),
    # (3, ('Empire')),
    # (4, ('Indépendant')),
    # )

    # GOUVERNEMENT_CHOICES = (
    # (5, ('Anarchie')),
    # (6, ('Impériale')),
    # (7, ('Patronage')),
    # (8, ('Colonie pénitentiaire')),
    # (9, ('Théocratie')),
    # (10, ('Colonie')),
    # (11, ('Communisme')),
    # (12, ('Confédération')),
    # (13, ('Coopérative')),
    # (14, ('Entreprise')),
    # (15, ('Démocratie')),
    # (16, ('Dictature')),
    # (17, ('Féodal')),
    # )

    # nom = models.CharField(max_length=100)
    # coordonnes = models.IntegerField()

    # établissement du choix multiple
    # allegeance = MultiSelectField(
    # title=models.CharField(max_length=200),
    # choices=ALLEGEANCE_CHOICES,
    # default=4,
    # max_choices=1,
    # )

    # def __str__(self):
    # return self.allegeance

    # établissement du choix multiple

    # gouvernement = MultiSelectField(
    # title=models.CharField(max_length=200),
    # choices=GOUVERNEMENT_CHOICES,
    # default=14,
    # max_choices=1,
    # )

    # def __unicode__(self):
    # return self.__str__()


# Table des types de corps
class CelestialType(models.Model):
    Nom = models.CharField(max_length=100)

    def __str__(self):
        return f"{self.Nom}"


# Table des corps célestes
class Celestial(models.Model):
    # Ce champ indique à quel système (le plus petit possible) le corps appartient, dans la table "system"
    # system = models.ForeignKey(System, on_delete=models.CASCADE, blank=True, null=True)
    fk_system_origine = models.ForeignKey(System, on_delete=models.CASCADE, related_name="emplacement", blank=True,
                                          null=True)
    # Ce champ indique à quel type de corps le corps correspond, dans la table "type"
    fk_type = models.ForeignKey(CelestialType, on_delete=models.CASCADE, related_name="type", blank=True, null=True)
    nom = models.TextField(null=True)
    distance_au_sol = models.IntegerField()
    masse = models.IntegerField()
    rayon = models.IntegerField()
    gravite = models.IntegerField()
    temperature = models.IntegerField()
    volcanisme = models.TextField(null=True)
    atmosphere = models.TextField(null=True)
    composition = models.TextField(null=True)
    period_orbital = models.IntegerField()
    inclinaison_orbital = models.IntegerField()
    station = models.TextField(null=True)
    distance = models.IntegerField()

    def name(self):
        return self.nom

    def system(self):
        return self.fk_system_origine

    def __str__(self):
        return f"{self.nom}"
