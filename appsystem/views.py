from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth import logout, authenticate, login
from appsystem.form import *
from appsystem.models import System, CelestialType
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


# Fonction utilisee pour verifier l'etat de connection d'un utilisateur. Utilisee par toutes les autres fonctions
def connect(request):
    adm = 0  # Compte administrateur
    connecte = 0  # Connecte
    username = 0  # id de l'utilisateur
    if request.session.__contains__('appsystem_connect_state') == True:
        if request.session['appsystem_connect_state'] == True:  # si l'utilisateur est connecte
            connecte = 1

            username = User.objects.get(username=request.session['appsystem_user'])
            if str(username.admin) == str(1):
                adm = 1

    return ({"connecte": connecte, "adm": adm, "username": username})


def recherche(request) :
    Celest = Celestial.objects.all()
    Syst = System.objects.all()
    return render (request, 'Templates/search.html', {"Celest" : Celest, 'Syst': Syst})


def system_add(request):
    if request.user.is_authenticated:
        form = SystemForm()
        return render(request, 'templates/system_add.html', {"form": form})
    else:
        return redirect('login')


def celestial_add(request):  # Fonctionne (normalement)
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = CelestialForm(request.POST or None)

            if form.is_valid():
                form.save()
                return redirect('system_list')

        form = CelestialForm()
        return render(request, 'templates/celestial_add.html', {'form': form})
    else:
        return redirect('login')


def home(request):
    return render(request, "templates/home.html", {})


def search(request):
    return render(request, "templates/search.html", {})


def system_list(request):
    if request.method == 'POST':
        form = SystemForm(request.POST or None)

        if form.is_valid():
            new_system = System()
            new_system.name = form.cleaned_data['name']
            #new_system.image = form.cleaned_data['image']
            new_system.allegiance = form.cleaned_data['allegiance']
            new_system.coordinates = form.cleaned_data['coordinates']
            new_system.population = int(form.cleaned_data['population'])
            new_system.ground_distance = int(form.cleaned_data['ground_distance'])
            new_system.state = form.cleaned_data['state']
            new_system.economy = form.cleaned_data['economy']
            new_system.faction = form.cleaned_data['faction']
            new_system.security = form.cleaned_data['security']
            new_system.government = form.cleaned_data['government']
            new_system.save()

    systems = System.objects.all()
    return render(request, "templates/system_list.html", {'systems': systems})


def system_details(request, system_id):
    system = System.objects.get(pk=system_id)
    return render(request, 'templates/system_details.html', {'system': system})


def celestial_list(request):
    return render(request, "templates/system_details.html", {})


def celestialtype_add(request):  # Fonctionne
    if request.user.is_authenticated:
        typ = CelestialType.objects.all()  # Pour afficher la liste des types déjà présents dans la table
        form_type = TypeForm()
        return render(request, 'templates/celestialtypes_add.html', {"types": typ, 'form': form_type})
    else:
        return redirect('login')


# Connexion utilisateur
def sign_in(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return redirect('system_list')
    else:
        return render(request, "templates/login.html", {})


# Creation d'un compte utilisateur + authentification
class sign_up(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/sign_up.html'

def deconnexion(request):
    request.session['appsystem_connect_state']=False
    return render (request,"templates/deconnexion.html")


###########
## Controller methods for Systems

def systems_index(request):
    systems = System.objects.all()
    return render(  request,
                    "templates/systems/index.html",
                    { 'systems': systems }   )


def systems_show(request, system_id):
    system = get_object_or_404(System, pk=system_id)
    return render(  request,
                    'templates/systems/show.html',
                    { 'system': system }    )


def systems_new(request):
    system_form = SystemForm()
    return render(request, 'templates/systems/new.html',
                  {"system_form": system_form})


def create_system(request):
    if request.method == 'POST':
        system = SystemForm(request.POST)
        if system.is_valid():
            system.save()
    else:
        system = SystemForm

    return render(  request,
                    'templates/home.html',
                    { 'system_form': system }  )


###########
## Controller methods for Astronomical Bodies
def astronomical_bodies_index(request):
    astronomical_bodies = Celestial.objects.all()
    return render(  request,
                    "templates/astronomical_bodies/system_list.html",
                    { 'astronomical_bodies': astronomical_bodies }   )


def astronomical_bodies_show(request, astronomical_body_id):
    astronomical_body = get_object_or_404(Celestial, pk=astronomical_body_id)
    return render(  request,
                    'templates/astronomical_bodies/show.html',
                    { 'astronomical_body': astronomical_body }    )


def choices_add(request):
    return render(request, "templates/choices_add.html", {})