from django.contrib import admin
from appsystem.models import System, CelestialType, Celestial

# Register your models here.
admin.site.register(System)
admin.site.register(CelestialType)
admin.site.register(Celestial)