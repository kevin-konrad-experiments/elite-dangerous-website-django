from django.apps import AppConfig


class AppsystemConfig(AppConfig):
    name = 'appsystem'
