from django.forms import ModelForm
from appsystem.models import *
from django import forms


# Formulaire pour rentrer un nouveau corps céleste utilisation de tous les champs du model avec nom d'affichage.Creation automatique
class CelestialForm(ModelForm):
    class Meta:
        model = Celestial
        fields = '__all__'
        labels = {'nom': 'Nom', #permet d'afficher le nom voulu
                  'distance_au_sol': 'Distance au soleil',
                  'masse': 'Masse',
                  'rayon': 'Rayon',
                  'gravite': 'Gravité',
                  'temperature': 'Température',
                  'volcanisme': 'Volcanisme',
                  'atmosphere': 'Atmosphère',
                  'composition': 'Composition',
                  'period_orbital': 'Période orbitale',
                  'inclinaison_orbital': 'Inclinaison orbitale',
                  'station': 'Station',
                  'distance': 'Distance'}


# Formulaire pour rentrer un nouveau système. Chaque champs du formulaire à la main
class SystemForm(forms.Form):
    ALLEGEANCE_CHOICES = (
        (1, ('Fédération')),
        (2, ('Alliance')),
        (3, ('Empire')),
        (4, ('Indépendant')),
    )

    GOUVERNEMENT_CHOICES = (
        (5, ('Anarchie')),
        (6, ('Impériale')),
        (7, ('Patronage')),
        (7, ('Colonie pénitentiaire')),
        (8, ('Théocratie')),
        (9, ('Colonie')),
        (10, ('Communisme')),
        (11, ('Confédération')),
        (12, ('Coopérative')),
        (13, ('Entreprise')),
        (14, ('Démocratie')),
        (15, ('Dictature')),
        (16, ('Féodal')),
    )

    name = forms.CharField()
    #image = forms.ImageField()
    allegiance = forms.ChoiceField(choices=ALLEGEANCE_CHOICES, widget=forms.RadioSelect)
    coordinates = forms.CharField()
    population = forms.IntegerField()
    ground_distance = forms.IntegerField()
    state = forms.CharField()
    economy = forms.CharField()
    faction = forms.CharField()
    security = forms.CharField()
    government = forms.ChoiceField(choices=GOUVERNEMENT_CHOICES, widget=forms.RadioSelect)

    class Meta:
        model = System
        fields = '__all__'
        labels = {'name': 'Nom',
                  'coordinates': 'Coordonnées',
                  'allegiance': 'Allégeance',
                  'government': 'Gouvernement',
                  'population': 'Population',
                  'ground_distance': 'Distance au Soleil',
                  'state': 'État',
                  'economy': 'Économie',
                  'faction': 'Faction',
                  'security': 'Sécurité',
                  'fk_system_parent': 'Système dans lequel le système se trouve'}


# Formulaire pour rentrer un nouveau type de corps céleste
class TypeForm(ModelForm):
    class Meta:
        model = CelestialType
        fields = '__all__'
        labels = {'nom': 'Entrez le type de corps céleste auquel correspond votre nouveau corps céleste'}