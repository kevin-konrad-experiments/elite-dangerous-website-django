from django.contrib import admin
from django.urls import path, include
from appsystem.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home, name='home'),
    path('sign-up/', sign_up.as_view(), name='sign_up'),
    path('deconnexion/', deconnexion, name='Deconnexion'),
    path('', include('django.contrib.auth.urls')),

    path('search/', recherche, name='search'),
    path('system/', system_list, name='system_list'),
    path('system/add/', system_add, name='system_add'),
    path('system/<int:system_id>/', system_details, name='system_details'),
    path('celestial/', celestial_list, name='celestial_list'),
    path('choices/add/',choices_add, name='choices_add'),
    path('celestial/add/', celestial_add, name='celestial_add'),
    path('celestialtypes/add', celestialtype_add, name='celestialtype_add'),

    path('systems/', systems_index, name='systems'),
    path('systems/<int:system_id>/', systems_show, name='systems'),
    path('systems/new/', systems_new, name='new_system'),
    path('systems/create/', create_system, name='create_system'),
    path('astronomical_bodies/', astronomical_bodies_index, name='astronomical_bodies'),
    path('astronomical_bodies/<int:astronomical_body_id>/', astronomical_bodies_show, name='astronomical_bodies')
]
